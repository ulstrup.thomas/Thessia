https://scrutinizer-ci.com/gp/Thessia/badges/quality-score.png?b=master&s=61412094cbd1ecf596f6b122b049fab86ad50074
https://scrutinizer-ci.com/gp/Thessia/badges/coverage.png?b=master&s=2efb42e9fde2b3c214c32d3157109f03d157e29e
https://scrutinizer-ci.com/gp/Thessia/badges/build.png?b=master&s=120fbbcaa40104c3fe84a18d6216f2e36a6808a3

# Thessia

> _[Embrace Eternity...] (https://www.youtube.com/watch?v=vMEWIl_WwVA)_

Thessia is a small framework to build websites.
It uses Slim3 and several Symfony packages

# Requirements
PHP7

Redis

MySQL/MariaDB

# License

MIT Licensed